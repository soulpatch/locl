package com.soulpatch.locl.network;

import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * All REST-ful API calls should be made using this class
 *
 * @author Akshay Viswanathan
 */
public class RestCallerTask extends AsyncTask<QueryParam, Integer, NetworkResponse> {
    private static final String BASE_URL = "https://api.seatgeek.com/2/";
    private final String TAG = RestCallerTask.class.getSimpleName();
    private final RestCallback mRestCallback;
    private final String mRequestURL;
    private Exception mException;
    private final String CLIENT_ID = "OTk0OTQ4NnwxNTEzMTY0OTE2LjA4";

    /**
     * Constructor that takes all the necessary arguments
     *
     * @param requestURL url that comes after the base url (like: /jersey/rest). NOTE: This is different from the params
     * @param restCallback {@link RestCallback} call back to be called in onPostExecute
     */
    public RestCallerTask(@NonNull final String requestURL,
                          @NonNull final RestCallback restCallback) {
        //noinspection ConstantConditions
        if (restCallback == null) {
            throw new IllegalArgumentException("None of the arguments passed to the constructor can be null");
        }

        mRestCallback = restCallback;
        mRequestURL = requestURL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mRestCallback.onPreExecute();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onProgressUpdate(final Integer... values) {
        super.onProgressUpdate(values);
        Log.i(TAG, "Progress " + String.valueOf(values[0]));
        mRestCallback.onPublishProgress(values);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected NetworkResponse doInBackground(final QueryParam... params) {
        final Uri.Builder uriBuilder = Uri.parse(BASE_URL).buildUpon();

        if (!TextUtils.isEmpty(mRequestURL)) {
            uriBuilder.appendPath(mRequestURL);
        }

        if (params != null) {
            for (final QueryParam queryParam : params) {
                if (queryParam != null) {
                    uriBuilder.appendQueryParameter(queryParam.paramKey, queryParam.paramValue);
                }
            }
        }

        //Finally add the api key if one is provided
        uriBuilder.appendQueryParameter("client_id", CLIENT_ID);

        final URL finalUrl;
        HttpURLConnection urlConnection = null;
        try {
            finalUrl = new URL(uriBuilder.build().toString());
            System.out.println("URL*** :" + finalUrl);
            urlConnection = (HttpURLConnection) finalUrl.openConnection();
            urlConnection.connect();

            final int status = urlConnection.getResponseCode();
            final NetworkResponse networkResponse = new NetworkResponse();
            switch (status) {
                case HttpURLConnection.HTTP_NO_CONTENT:
                case HttpURLConnection.HTTP_CREATED:
                case HttpURLConnection.HTTP_OK:
                    final InputStream inputStream = urlConnection.getInputStream();
                    if (inputStream == null) {
                        Log.e(TAG, "Connection to " + finalUrl.toString() + " returned null input stream");
                        return null;
                    }

                    final InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    final BufferedReader in = new BufferedReader(inputStreamReader);

                    String inputLine;
                    final StringBuilder finalOutput = new StringBuilder();

                    //TODO Publish progress
                    while ((inputLine = in.readLine()) != null) {
                        finalOutput.append(inputLine);
                    }
                    networkResponse.responseData = finalOutput.toString();
                    networkResponse.statusCode = status;

                    in.close();
                    return networkResponse;
                case HttpURLConnection.HTTP_FORBIDDEN:
                    networkResponse.statusCode = HttpURLConnection.HTTP_FORBIDDEN;
                    final InputStream errorStream = urlConnection.getErrorStream();
                    if (errorStream == null) {
                        Log.e(TAG, "Connection to " + finalUrl.toString() + " returned null input stream");
                        return null;
                    }

                    final InputStreamReader errorStreamReader = new InputStreamReader(errorStream);
                    final BufferedReader errorBr = new BufferedReader(errorStreamReader);

                    String errorLine;
                    final StringBuilder finalError = new StringBuilder();

                    while ((errorLine = errorBr.readLine()) != null) {
                        finalError.append(errorLine);
                    }
                    networkResponse.responseData = finalError.toString();
                    return networkResponse;
                case HttpURLConnection.HTTP_UNAUTHORIZED:
                    networkResponse.statusCode = HttpURLConnection.HTTP_UNAUTHORIZED;
                    return networkResponse;
                default:
                    networkResponse.statusCode = status;
                    return networkResponse;
            }
        } catch (final Exception exception) {
            mException = exception;
            mRestCallback.onResponse(null, mException);
            Log.e(TAG, exception.getMessage());
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return null;
    }

    /**
     * Calls the {@link RestCallback} with the response object received <p> {@inheritDoc}
     */
    @Override
    protected void onPostExecute(final NetworkResponse responseObj) {
        if (responseObj != null) {
            switch (responseObj.statusCode) {
                case HttpURLConnection.HTTP_NO_CONTENT:
                case HttpURLConnection.HTTP_CREATED:
                case HttpURLConnection.HTTP_OK:
                    mRestCallback.onResponse(responseObj, mException);
                    break;
                default:
                    mRestCallback.onErrorResponse(new NetworkResponse());
            }
        } else {
            mRestCallback.onErrorResponse(new NetworkResponse());
        }
    }
}