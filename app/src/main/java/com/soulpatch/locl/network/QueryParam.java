package com.soulpatch.locl.network;

/**
 * Represents a Database parameter object needed to add parameters to a DB query
 *
 * @author Akshay Viswanathan.
 */
public class QueryParam {
    public String paramKey;
    public String paramValue;

    public QueryParam(final String paramKey, final String paramValue) {
        this.paramKey = paramKey;
        this.paramValue = paramValue;
    }
}
