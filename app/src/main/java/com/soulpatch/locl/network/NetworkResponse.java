package com.soulpatch.locl.network;

import java.util.Map;

/**
 * @author Akshay Viswanathan
 */
public class NetworkResponse {
    public String detail;
    public String responseData;
    public Map<String, String> responseHeaders;
    public int statusCode;
}