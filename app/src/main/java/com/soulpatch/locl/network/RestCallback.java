package com.soulpatch.locl.network;

/**
 * Callback to call into from the onPostExecute of the RestCallerTask
 *
 * @author Akshay Viswanathan
 */
public interface RestCallback {
    /**
     * Method to return the response of the async call
     *
     * @param responseObj {@link Object} returned as a response
     * @param exception {@link Exception} encountered during doInBackground operation to be sent to the user to take necessary action
     */
    void onResponse(final NetworkResponse responseObj, final Exception exception);

    void onPublishProgress(Integer... values);

    void onErrorResponse(final NetworkResponse response);

    void onPreExecute();
}
