package com.soulpatch.locl.network;

import com.soulpatch.locl.datamodel.Event;
import com.soulpatch.locl.datamodel.Meta;
import com.soulpatch.locl.datamodel.Performer;
import com.soulpatch.locl.datamodel.Venue;

import java.util.List;

/**
 * Parent class for seatgeek response object
 *
 * @author Akshay Viswanathan
 */
public class SeatGeekResponse {
    private Meta meta;
    private List<Venue> venues;
    private List<Performer> performers;
    private List<Event> events;

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(final List<Event> events) {
        this.events = events;
    }

    public List<Performer> getPerformers() {
        return performers;
    }

    public void setPerformers(final List<Performer> performers) {
        this.performers = performers;
    }

    public List<Venue> getVenues() {
        return venues;
    }

    public void setVenues(final List<Venue> venues) {
        this.venues = venues;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(final Meta meta) {
        this.meta = meta;
    }
}
