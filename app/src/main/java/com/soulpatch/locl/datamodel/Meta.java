package com.soulpatch.locl.datamodel;

import java.io.Serializable;

/**
 * POJO representing the meta-data received from seatgeek
 *
 * @author Akshay Viswanathan
 */
public class Meta implements Serializable {
    private String per_page;
    private String took;
    private String total;
    private String page;
    private String geolocation;

    public String getPer_page() {
        return per_page;
    }

    public void setPer_page(final String per_page) {
        this.per_page = per_page;
    }

    public String getTook() {
        return took;
    }

    public void setTook(final String took) {
        this.took = took;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(final String total) {
        this.total = total;
    }

    public String getPage() {
        return page;
    }

    public void setPage(final String page) {
        this.page = page;
    }

    public String getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(final String geolocation) {
        this.geolocation = geolocation;
    }
}
