package com.soulpatch.locl.datamodel;

import java.io.Serializable;

/**
 * POJO representing a division that performers belong to
 *
 * @author Akshay Viswanathan
 */
class Division implements Serializable {
    private String division_level;
    private String short_name;
    private String display_name;
    private String display_type;
    private String taxonomy_id;
    private String slug;

    public String getDivision_level() {
        return division_level;
    }

    public void setDivision_level(final String division_level) {
        this.division_level = division_level;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(final String short_name) {
        this.short_name = short_name;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(final String display_name) {
        this.display_name = display_name;
    }

    public String getDisplay_type() {
        return display_type;
    }

    public void setDisplay_type(final String display_type) {
        this.display_type = display_type;
    }

    public String getTaxonomy_id() {
        return taxonomy_id;
    }

    public void setTaxonomy_id(final String taxonomy_id) {
        this.taxonomy_id = taxonomy_id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(final String slug) {
        this.slug = slug;
    }
}
