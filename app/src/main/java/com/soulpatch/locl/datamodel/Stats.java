package com.soulpatch.locl.datamodel;

import java.io.Serializable;

/**
 * POJO representing statistics
 *
 * @author Akshay Viswanathan
 */
public class Stats implements Serializable {
    private String average_price;
    private String highest_price;
    private String listing_count;
    private String lowest_price_good_deals;
    private String lowest_price;

    public String getAverage_price() {
        return average_price;
    }

    public void setAverage_price(final String average_price) {
        this.average_price = average_price;
    }

    public String getHighest_price() {
        return highest_price;
    }

    public void setHighest_price(final String highest_price) {
        this.highest_price = highest_price;
    }

    public String getListing_count() {
        return listing_count;
    }

    public void setListing_count(final String listing_count) {
        this.listing_count = listing_count;
    }

    public String getLowest_price_good_deals() {
        return lowest_price_good_deals;
    }

    public void setLowest_price_good_deals(final String lowest_price_good_deals) {
        this.lowest_price_good_deals = lowest_price_good_deals;
    }

    public String getLowest_price() {
        return lowest_price;
    }

    public void setLowest_price(final String lowest_price) {
        this.lowest_price = lowest_price;
    }
}
