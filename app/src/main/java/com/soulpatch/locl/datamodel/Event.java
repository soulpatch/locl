package com.soulpatch.locl.datamodel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * POJO representing an event
 *
 * @author Akshay Viswanathan
 */
public class Event implements Serializable {
    @SerializedName("time_tbd")
    private boolean timeTbd;
    private Venue venue;
    private String url;
    private List<Taxonomy> taxonomies;
    @SerializedName("datetime_local")
    private String dateTimeLocal;
    private List<Performer> performers;
    private String score;
    @SerializedName("visible_until_utc")
    private String visibleUntilUtc;
    @SerializedName("date_tbd")
    private String dateTbd;
    @SerializedName("datetime_tbd")
    private String datetimeTbd;
    @SerializedName("short_title")
    private String shortTitle;
    @SerializedName("announce_date")
    private String announceDate;
    private String title;
    private long id;
    private String type;
    private Stats stats;
    @SerializedName("is_open")
    private String isOpen;
    private String popularity;
    @SerializedName("datetimeUtc")
    private String datetimeUtc;
    @SerializedName("created_at")
    private String createdAt;
    private String status;

    public boolean isTimeTbd() {
        return timeTbd;
    }

    public void setTimeTbd(final boolean timeTbd) {
        this.timeTbd = timeTbd;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(final Venue venue) {
        this.venue = venue;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public List<Taxonomy> getTaxonomies() {
        return taxonomies;
    }

    public void setTaxonomies(final List<Taxonomy> taxonomies) {
        this.taxonomies = taxonomies;
    }

    public String getDateTimeLocal() {
        return dateTimeLocal;
    }

    public void setDateTimeLocal(final String dateTimeLocal) {
        this.dateTimeLocal = dateTimeLocal;
    }

    public List<Performer> getPerformers() {
        return performers;
    }

    public void setPerformers(final List<Performer> performers) {
        this.performers = performers;
    }

    public String getScore() {
        return score;
    }

    public void setScore(final String score) {
        this.score = score;
    }

    public String getVisibleUntilUtc() {
        return visibleUntilUtc;
    }

    public void setVisibleUntilUtc(final String visibleUntilUtc) {
        this.visibleUntilUtc = visibleUntilUtc;
    }

    public String getDateTbd() {
        return dateTbd;
    }

    public void setDateTbd(final String dateTbd) {
        this.dateTbd = dateTbd;
    }

    public String getDatetimeTbd() {
        return datetimeTbd;
    }

    public void setDatetimeTbd(final String datetimeTbd) {
        this.datetimeTbd = datetimeTbd;
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public void setShortTitle(final String shortTitle) {
        this.shortTitle = shortTitle;
    }

    public String getAnnounceDate() {
        return announceDate;
    }

    public void setAnnounceDate(final String announceDate) {
        this.announceDate = announceDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(final Stats stats) {
        this.stats = stats;
    }

    public String getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(final String isOpen) {
        this.isOpen = isOpen;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(final String popularity) {
        this.popularity = popularity;
    }

    public String getDatetimeUtc() {
        return datetimeUtc;
    }

    public void setDatetimeUtc(final String datetimeUtc) {
        this.datetimeUtc = datetimeUtc;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(final String createdAt) {
        this.createdAt = createdAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }
}
