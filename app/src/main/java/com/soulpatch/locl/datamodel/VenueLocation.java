package com.soulpatch.locl.datamodel;

import java.io.Serializable;

/**
 * POJO representing the location co-ordinates of an event
 *
 * @author Akshay Viswanathan
 */
public class VenueLocation implements Serializable {
    private String lat;
    private String lon;

    public String getLat() {
        return lat;
    }

    public void setLat(final String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(final String lon) {
        this.lon = lon;
    }
}
