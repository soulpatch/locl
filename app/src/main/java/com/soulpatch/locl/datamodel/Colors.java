package com.soulpatch.locl.datamodel;

import java.io.Serializable;

/**
 * POJO representing colors for a {@link Performer}
 *
 * @author Akshay Viswanathan
 */
public class Colors implements Serializable {
    private String[] primary;
    private String[] all;
    private String iconic;

    public String[] getPrimary() {
        return primary;
    }

    public void setPrimary(final String[] primary) {
        this.primary = primary;
    }

    public String[] getAll() {
        return all;
    }

    public void setAll(final String[] all) {
        this.all = all;
    }

    public String getIconic() {
        return iconic;
    }

    public void setIconic(final String iconic) {
        this.iconic = iconic;
    }
}
