package com.soulpatch.locl.datamodel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.DecimalFormat;

/**
 * POJO representing a Venue
 *
 * @author Akshay Viswanathan
 */
public class Venue implements Serializable {
    @SerializedName("has_upcoming_events")
    private boolean hasUpcomingEvents;
    @SerializedName("name_v2")
    private String nameV2;
    private String url;
    private String slug;
    private VenueLocation location;
    private float score;
    private String country;
    private String timezone;
    @SerializedName("display_location")
    private String displayLocation;
    @SerializedName("num_upcoming_events")
    private int numUpcomingEvents;
    private long id;
    @SerializedName("extended_address")
    private String extendedAddress;
    @SerializedName("postal_code")
    private String postalCode;
    private String name;
    private String city;
    private String popularity;
    private String state;
    private String address;

    public boolean hasUpcomingEvents() {
        return hasUpcomingEvents;
    }

    public void setHasUpcomingEvents(final boolean hasUpcomingEvents) {
        this.hasUpcomingEvents = hasUpcomingEvents;
    }

    public String getNameV2() {
        return nameV2;
    }

    public void setNameV2(final String nameV2) {
        this.nameV2 = nameV2;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(final String slug) {
        this.slug = slug;
    }

    public VenueLocation getLocation() {
        return location;
    }

    public void setLocation(final VenueLocation location) {
        this.location = location;
    }

    public String getScore() {
        final DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setMaximumFractionDigits(2);
        return decimalFormat.format(score * 100);
    }

    public void setScore(final float score) {
        this.score = score;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(final String country) {
        this.country = country;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(final String timezone) {
        this.timezone = timezone;
    }

    public String getDisplayLocation() {
        return displayLocation;
    }

    public void setDisplayLocation(final String displayLocation) {
        this.displayLocation = displayLocation;
    }

    public int getNumUpcomingEvents() {
        return numUpcomingEvents;
    }

    public void setNumUpcomingEvents(final int numUpcomingEvents) {
        this.numUpcomingEvents = numUpcomingEvents;
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getExtendedAddress() {
        return extendedAddress;
    }

    public void setExtendedAddress(final String extendedAddress) {
        this.extendedAddress = extendedAddress;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(final String postalCode) {
        this.postalCode = postalCode;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(final String popularity) {
        this.popularity = popularity;
    }

    public String getState() {
        return state;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }
}
