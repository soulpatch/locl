package com.soulpatch.locl.datamodel;

import java.io.Serializable;

/**
 * @author Akshay Viswanathan
 */
class Taxonomy implements Serializable {
    private String name;
    private String parent_id;
    private String id;
}
