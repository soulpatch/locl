package com.soulpatch.locl.datamodel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.List;

/**
 * POJO representing a performer / artist
 *
 * @author Akshay Viswanathan
 */
public class Performer implements Serializable {
    @SerializedName("has_upcoming_events")
    private boolean hasUpcomingEvents;
    @SerializedName("short_name")
    private String shortName;
    private URL image;
    private URL url;
    private String slug;
    @SerializedName("image_attribution")
    private String imageAttribution;
    private int id;
    private float score;
    @SerializedName("image_license")
    private String imageLicense;
    private Colors colors;
    private Image images;
    private List<Taxonomy> taxonomies;
    @SerializedName("num_upcoming_events")
    private int numUpcomingEvents;
    @SerializedName("home_venue_id")
    private String homeVenueId;
    private String primary;
    private List<Division> divisions;
    private String type;
    private Stats stats;
    private String name;
    private String popularity;

    public boolean hasUpcomingEvents() {
        return hasUpcomingEvents;
    }

    public void setHasUpcomingEvents(final boolean hasUpcomingEvents) {
        this.hasUpcomingEvents = hasUpcomingEvents;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(final String shortName) {
        this.shortName = shortName;
    }

    public URL getImage() {
        return image;
    }

    public void setImage(final URL image) {
        this.image = image;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(final URL url) {
        this.url = url;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(final String slug) {
        this.slug = slug;
    }

    public String getImageAttribution() {
        return imageAttribution;
    }

    public void setImageAttribution(final String imageAttribution) {
        this.imageAttribution = imageAttribution;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getScore() {
        final DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setMaximumFractionDigits(2);
        return decimalFormat.format(score * 100);
    }

    public void setScore(final float score) {
        this.score = score;
    }

    public String getImageLicense() {
        return imageLicense;
    }

    public void setImageLicense(final String imageLicense) {
        this.imageLicense = imageLicense;
    }

    public Colors getColors() {
        return colors;
    }

    public void setColors(final Colors colors) {
        this.colors = colors;
    }

    public Image getImages() {
        return images;
    }

    public void setImages(final Image images) {
        this.images = images;
    }

    public int getNumUpcomingEvents() {
        return numUpcomingEvents;
    }

    public void setNumUpcomingEvents(final int numUpcomingEvents) {
        this.numUpcomingEvents = numUpcomingEvents;
    }

    public String getHomeVenueId() {
        return homeVenueId;
    }

    public void setHomeVenueId(final String homeVenueId) {
        this.homeVenueId = homeVenueId;
    }

    public String getPrimary() {
        return primary;
    }

    public void setPrimary(final String primary) {
        this.primary = primary;
    }

    public List<Division> getDivisions() {
        return divisions;
    }

    public void setDivisions(final List<Division> divisions) {
        this.divisions = divisions;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(final String popularity) {
        this.popularity = popularity;
    }

    public List<Taxonomy> getTaxonomies() {
        return taxonomies;
    }

    public void setTaxonomies(final List<Taxonomy> taxonomies) {
        this.taxonomies = taxonomies;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(final Stats stats) {
        this.stats = stats;
    }
}
