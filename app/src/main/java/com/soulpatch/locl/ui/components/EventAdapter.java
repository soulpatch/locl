package com.soulpatch.locl.ui.components;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.soulpatch.locl.R;
import com.soulpatch.locl.Utils;
import com.soulpatch.locl.datamodel.Event;
import com.soulpatch.locl.datamodel.VenueLocation;
import com.soulpatch.locl.ui.EventDetailFragment;

import java.util.List;

/**
 * Adapter for the {@link RecyclerView} representing the list of {@link Event}
 *
 * @author Akshay Viswanathan
 */
public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {
    private final Context mContext;
    private final LoadMoreListener mLoadMoreListener;
    private final List<Event> mValues;

    public EventAdapter(final Context context, final List<Event> events, final LoadMoreListener loadMoreListener) {
        mContext = context;
        mValues = events;
        mLoadMoreListener = loadMoreListener;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final CardView cardView = (CardView) LayoutInflater.from(mContext).inflate(R.layout.cardview_event, parent, false);
        return new ViewHolder(cardView);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Event event = mValues.get(position);
        final VenueLocation location = event.getVenue().getLocation();
        holder.initializeMapView();
        holder.name.setText(event.getTitle());
        holder.address.setText(mContext.getString(R.string.address, event.getVenue().getName(), event.getVenue().getAddress(), event.getVenue().getCity()));

        if (!event.isTimeTbd()) {
            holder.date.setText(Utils.dateFormatter(event.getDateTimeLocal()));
            holder.date.setTextColor(mContext.getResources().getColor(android.R.color.tab_indicator_text));
        } else {
            holder.date.setText(R.string.to_be_determined_label);
            holder.date.setTextColor(mContext.getResources().getColor(android.R.color.holo_red_dark));
        }

        holder.mapView.setTag(location);

        if (holder.map != null) {
            // The map is already ready to be used
            setMapLocation(holder.map, location);
        }

        if (getItemCount() > 5 && position == getItemCount() - 1) {
            mLoadMoreListener.onLoadMore();
        }

        holder.mRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final FragmentTransaction fragmentTransaction = ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, EventDetailFragment.getInstance(event), null).addToBackStack(null).commit();
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {
        private final CardView mRootView;
        private final TextView name;
        private final TextView address;
        private final MapView mapView;
        private final TextView date;
        GoogleMap map;

        ViewHolder(final View rootView) {
            super(rootView);
            mRootView = (CardView) rootView;
            name = rootView.findViewById(R.id.event_name);
            address = rootView.findViewById(R.id.event_venue_address);
            date = rootView.findViewById(R.id.event_date);
            mapView = rootView.findViewById(R.id.map_view);
        }

        @Override
        public void onMapReady(final GoogleMap googleMap) {
            MapsInitializer.initialize(mContext);
            map = googleMap;
            final VenueLocation data = (VenueLocation) mapView.getTag();
            if (data != null) {
                setMapLocation(map, data);
            }
        }

        /**
         * Initialises the MapView by calling its lifecycle methods.
         */
        void initializeMapView() {
            if (mapView != null) {
                // Initialise the MapView
                mapView.onCreate(null);
                // Set the map ready callback to receive the GoogleMap object
                mapView.getMapAsync(this);
            }
        }
    }

    /**
     * Displays a {@link VenueLocation} on a
     * {@link com.google.android.gms.maps.GoogleMap}.
     * Adds a marker and centers the camera on the NamedLocation with the normal map type.
     */
    private static void setMapLocation(final GoogleMap map, final VenueLocation data) {
        //TODO change the VenueLocation to automatically convert the location data coming in JSON to Doubles at the timer of parsing
        final LatLng location = new LatLng(Double.valueOf(data.getLat()), Double.valueOf(data.getLon()));
        // Add a marker for this item and set the camera
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 13f));
        map.addMarker(new MarkerOptions().position(location));

        // Set the map type back to normal.
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }
}