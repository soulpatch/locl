package com.soulpatch.locl.ui.components;

import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.soulpatch.locl.R;
import com.soulpatch.locl.datamodel.Venue;
import com.soulpatch.locl.ui.EventListFragment;

/**
 * Fragment that shows the details of an {@link Venue}
 *
 * @author Akshay Viswanathan
 */

public class VenueDetailFragment extends Fragment implements OnMapReadyCallback {
    private static final String BUNDLE_ARGS_VENUE = "BUNDLE_ARGS_VENUE";
    private Venue mVenue;
    private ImageView zoomIn;
    private ImageView zoomOut;
    private ImageView directions;
    private GoogleMap mGoogleMap;
    private LatLng mLocation;
    private float mZoomLevel = 13f;
    
    public static Fragment getInstance(final Venue venue) {
        final Bundle arguments = new Bundle();
        arguments.putSerializable(BUNDLE_ARGS_VENUE, venue);
        final VenueDetailFragment performerDetailFragment = new VenueDetailFragment();
        performerDetailFragment.setArguments(arguments);
        return performerDetailFragment;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle arguments = getArguments();
        if (arguments == null) {
            throw new IllegalStateException("Venue object needs to be passed to this fragment as a mandatory parameter");
        }

        mVenue = (Venue) arguments.getSerializable(BUNDLE_ARGS_VENUE);
    }

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        final LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.fragment_venue_detail, container, false);
        final TextView name = linearLayout.findViewById(R.id.venue_name);
        name.setText(mVenue.getName());
        final TextView score = linearLayout.findViewById(R.id.venue_score);
        score.setText(String.valueOf(getString(R.string.score, mVenue.getScore())));
        final TextView upcomingEvents = linearLayout.findViewById(R.id.venue_upcoming_events);
        zoomIn = linearLayout.findViewById(R.id.zoom_in);
        zoomOut = linearLayout.findViewById(R.id.zoom_out);
        directions = linearLayout.findViewById(R.id.directions);

        final SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.event_map);
        mapFragment.onCreate(null);
        mapFragment.getMapAsync(this);

        if (mVenue.hasUpcomingEvents()) {
            upcomingEvents.setText(getString(R.string.upcoming_events_venue, mVenue.getNumUpcomingEvents()));
            upcomingEvents.setTextColor(getResources().getColor(android.R.color.holo_blue_dark));
            upcomingEvents.setPaintFlags(upcomingEvents.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            upcomingEvents.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    final FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container, EventListFragment.getInstance(null, mVenue.getId()), null).addToBackStack(null).commit();
                }
            });
        } else {
            upcomingEvents.setText(R.string.no_upcoming_events_label);
            upcomingEvents.setTextColor(getActivity().getResources().getColor(android.R.color.holo_red_dark));
            upcomingEvents.setOnClickListener(null);
        }

        return linearLayout;
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mLocation = new LatLng(Double.valueOf(mVenue.getLocation().getLat()), Double.valueOf(mVenue.getLocation().getLon()));
        // Add a marker for this item and set the camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLocation, 13f));
        mGoogleMap.addMarker(new MarkerOptions().position(mLocation));

        // Set the map type back to normal.
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        zoomIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                mZoomLevel += 1f;
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLocation, mZoomLevel));
            }
        });

        zoomOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                mZoomLevel -= 1f;
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLocation, mZoomLevel));
            }
        });

        directions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final String uri = "http://maps.google.com/maps?daddr=" + mLocation.latitude + "," + mLocation.longitude;
                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
        });
    }
}
