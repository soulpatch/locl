package com.soulpatch.locl.ui.components;

/**
 * Interface to load more data when the user reaches the end of the list by scrolling
 *
 * @author Akshay Viswanathan
 */
public interface LoadMoreListener {
    void onLoadMore();
}
