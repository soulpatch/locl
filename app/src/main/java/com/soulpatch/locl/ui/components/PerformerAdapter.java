package com.soulpatch.locl.ui.components;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.soulpatch.locl.R;
import com.soulpatch.locl.datamodel.Performer;
import com.soulpatch.locl.ui.PerformerDetailFragment;

import java.util.HashMap;
import java.util.List;

/**
 * Adapter for {@link RecyclerView} to show a single {@link Performer} detail
 *
 * @author Akshay Viswanathan
 */
public class PerformerAdapter extends RecyclerView.Adapter<PerformerAdapter.ViewHolder> {
    private final Context mContext;
    private final LoadMoreListener mLoadMoreListener;
    private final List<Performer> mValues;
    private final HashMap<Integer, Bitmap> photoThumbnails = new HashMap<>();

    public PerformerAdapter(final Context context, final List<Performer> events, final LoadMoreListener loadMoreListener) {
        mContext = context;
        mValues = events;
        mLoadMoreListener = loadMoreListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final CardView cardView = (CardView) LayoutInflater.from(mContext).inflate(R.layout.cardview_performer, parent, false);
        return new ViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Performer performer = mValues.get(position);
        holder.name.setText(performer.getName());
        final Bitmap bitmap = photoThumbnails.get(performer.getId());

        final ImageLoadListener mImageLoadListener = new ImageLoadListener() {
            @Override
            public void onImageLoaded(final Bitmap bitmap) {
                holder.image.setImageBitmap(bitmap);
                photoThumbnails.put(performer.getId(), bitmap);
            }
        };

        if (bitmap == null) {
            //Get the bitmap from the URL
            if (performer.getImage() != null) {
                new ImageLoader(mImageLoadListener).execute(performer.getImage());
            }
        } else {
            holder.image.setImageBitmap(bitmap);
        }

        if (position == getItemCount() - 1) {
            mLoadMoreListener.onLoadMore();
        }

        holder.mRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final FragmentTransaction fragmentTransaction = ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, PerformerDetailFragment.getInstance(performer), null).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final CardView mRootView;
        private final TextView name;
        private final ImageView image;

        ViewHolder(final View rootView) {
            super(rootView);
            mRootView = (CardView) rootView;
            name = rootView.findViewById(R.id.performer_name);
            image = rootView.findViewById(R.id.performer_image);
        }
    }
}