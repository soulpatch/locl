package com.soulpatch.locl.ui.components;

import android.graphics.Bitmap;

/**
 * Call back for the {@link ImageLoader}
 *
 * @author Akshay Viswanathan
 */
public interface ImageLoadListener {
    void onImageLoaded(Bitmap bitmap);
}
