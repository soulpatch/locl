package com.soulpatch.locl.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.soulpatch.locl.R;
import com.soulpatch.locl.datamodel.Event;
import com.soulpatch.locl.network.NetworkResponse;
import com.soulpatch.locl.network.QueryParam;
import com.soulpatch.locl.network.RestCallback;
import com.soulpatch.locl.network.RestCallerTask;
import com.soulpatch.locl.network.SeatGeekResponse;
import com.soulpatch.locl.ui.components.EventAdapter;
import com.soulpatch.locl.ui.components.LoadMoreListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment that displays a list of {@link Event}s
 *
 * @author Akshay Viswanathan
 */
public class EventListFragment extends Fragment {
    private static final String BUNDLE_ARGS_PERFORMER = "BUNDLE_ARGS_PERFORMER";
    private static final String BUNDLE_ARGS_VENUE = "BUNDLE_ARGS_VENUE";
    private final List<Event> mCurItems = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private ProgressDialog mProgressDialog;
    private int mPage = 1;
    private String mPerformerSlug;
    private long mVenueID = -1;

    public static Fragment getInstance(final String performer, final long venue) {
        final Bundle arguments = new Bundle();
        arguments.putString(BUNDLE_ARGS_PERFORMER, performer);
        arguments.putLong(BUNDLE_ARGS_VENUE, venue);
        final EventListFragment eventDetailFragment = new EventListFragment();
        eventDetailFragment.setArguments(arguments);
        return eventDetailFragment;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle arguments = getArguments();
        if (arguments != null) {
            mPerformerSlug = arguments.getString(BUNDLE_ARGS_PERFORMER);
            mVenueID = arguments.getLong(BUNDLE_ARGS_VENUE);
        }

        final ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.event_list_title);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_main, container, false);
        mRecyclerView.setAdapter(new EventAdapter(getActivity(), mCurItems, new LoadMoreListener() {
            @Override
            public void onLoadMore() {
                loadData(mPage++);
            }
        }));

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        return mRecyclerView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onResume() {
        super.onResume();
        loadData(1);
    }

    private void loadData(final int page) {
        QueryParam queryParam = null;
        if (!TextUtils.isEmpty(mPerformerSlug)) {
            queryParam = new QueryParam("performers.slug", mPerformerSlug);
        } else if (mVenueID > -1) {
            queryParam = new QueryParam("venue.id", String.valueOf(mVenueID));
        }

        final RestCallerTask restCallerTask = new RestCallerTask("events", new RestCallback() {
            @Override
            public void onPreExecute() {
                mProgressDialog = new ProgressDialog(getActivity());
                mProgressDialog.setMessage("Loading data...");
                mProgressDialog.show();
            }

            @Override
            public void onResponse(final NetworkResponse responseObj, final Exception exception) {
                mProgressDialog.cancel();
                if (exception != null) {
                    Toast.makeText(getActivity(), "Received an error in response " + exception.getMessage(), Toast.LENGTH_LONG).show();
                } else if (responseObj != null) {
                    final Gson gson = new Gson();
                    final SeatGeekResponse eventResponse = gson.fromJson(responseObj.responseData, SeatGeekResponse.class);
                    mCurItems.addAll(eventResponse.getEvents());

                    if (page > 1) {
                        mRecyclerView.getAdapter().notifyItemRangeInserted(page * 10, 10);
                    } else {
                        mRecyclerView.getAdapter().notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onPublishProgress(final Integer... values) {
                //TODO Publish progress
            }

            @Override
            public void onErrorResponse(final NetworkResponse response) {
                mProgressDialog.cancel();
                Toast.makeText(getActivity(), "Received an error in response " + response.responseData, Toast.LENGTH_LONG).show();
            }
        });

        if (queryParam == null) {
            restCallerTask.execute(new QueryParam("page", String.valueOf(page)));
        } else {
            restCallerTask.execute(queryParam, new QueryParam("page", String.valueOf(page)));
        }
    }
}
