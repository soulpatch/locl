package com.soulpatch.locl.ui;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.soulpatch.locl.R;
import com.soulpatch.locl.datamodel.Performer;
import com.soulpatch.locl.network.NetworkResponse;
import com.soulpatch.locl.network.QueryParam;
import com.soulpatch.locl.network.RestCallback;
import com.soulpatch.locl.network.RestCallerTask;
import com.soulpatch.locl.network.SeatGeekResponse;
import com.soulpatch.locl.ui.components.LoadMoreListener;
import com.soulpatch.locl.ui.components.PerformerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment that displays a list of {@link Performer}s
 *
 * @author Akshay Viswanathan
 */
public class PerformerListFragment extends Fragment {
    private final List<Performer> mCurItems = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private ProgressDialog mProgressDialog;
    private int mPage = 1;

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.performer_list_title);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) inflater.inflate(R.layout.fragment_main, container, false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(new PerformerAdapter(getActivity(), mCurItems, new LoadMoreListener() {
            @Override
            public void onLoadMore() {
                loadData(mPage++);
            }
        }));
        return mRecyclerView;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData(1);
    }

    private void loadData(final int page) {
        new RestCallerTask("performers", new RestCallback() {
            @Override
            public void onPreExecute() {
                mProgressDialog = new ProgressDialog(getActivity());
                mProgressDialog.setMessage("Loading data...");
                mProgressDialog.show();
            }

            @Override
            public void onResponse(final NetworkResponse responseObj, final Exception exception) {
                mProgressDialog.cancel();
                if (exception != null) {
                    Toast.makeText(getActivity(), "Received an error in response " + exception.getMessage(), Toast.LENGTH_LONG).show();
                } else if (responseObj != null) {
                    final Gson gson = new Gson();
                    final SeatGeekResponse eventResponse = gson.fromJson(responseObj.responseData, SeatGeekResponse.class);
                    mCurItems.addAll(eventResponse.getPerformers());
                    mRecyclerView.getAdapter().notifyDataSetChanged();
                }
            }

            @Override
            public void onPublishProgress(final Integer... values) {
                //TODO Publish progress
            }

            @Override
            public void onErrorResponse(final NetworkResponse response) {
                Toast.makeText(getActivity(), "Received an error in response", Toast.LENGTH_LONG).show();
            }
        }).execute(new QueryParam("page", String.valueOf(page)));
    }
}
