package com.soulpatch.locl.ui;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.soulpatch.locl.ui.components.ImageLoadListener;
import com.soulpatch.locl.ui.components.ImageLoader;
import com.soulpatch.locl.R;
import com.soulpatch.locl.datamodel.Performer;

/**
 * Fragment that shows the details of an {@link Performer}
 *
 * @author Akshay Viswanathan
 */
public class PerformerDetailFragment extends Fragment {
    private static final String BUNDLE_ARGS_PERFORMER = "BUNDLE_ARGS_PERFORMER";
    private Performer mPerformer;
    private ImageView image;

    public static Fragment getInstance(final Performer performer) {
        final Bundle arguments = new Bundle();
        arguments.putSerializable(BUNDLE_ARGS_PERFORMER, performer);
        final PerformerDetailFragment performerDetailFragment = new PerformerDetailFragment();
        performerDetailFragment.setArguments(arguments);
        return performerDetailFragment;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle arguments = getArguments();
        if (arguments == null) {
            throw new IllegalStateException("Performer object needs to be passed to this fragment as a mandatory parameter");
        }

        mPerformer = (Performer) arguments.getSerializable(BUNDLE_ARGS_PERFORMER);
    }

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        final LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.fragment_performer_detail, container, false);
        image = linearLayout.findViewById(R.id.performer_image);
        final ImageLoadListener mImageLoadListener = new ImageLoadListener() {
            @Override
            public void onImageLoaded(final Bitmap bitmap) {
                image.setImageBitmap(bitmap);
            }
        };

        //Get the bitmap from the URL
        if (mPerformer.getImage() != null) {
            new ImageLoader(mImageLoadListener).execute(mPerformer.getImage());
        }

        final TextView name = linearLayout.findViewById(R.id.performer_name);
        name.setText(mPerformer.getName());
        final TextView score = linearLayout.findViewById(R.id.performer_score);
        score.setText(String.valueOf(getString(R.string.score, mPerformer.getScore())));
        final TextView upcomingEvents = linearLayout.findViewById(R.id.performer_upcoming_events);
        if (mPerformer.hasUpcomingEvents()) {
            upcomingEvents.setText(getString(R.string.upcoming_events_performer, mPerformer.getNumUpcomingEvents()));
            upcomingEvents.setTextColor(getResources().getColor(android.R.color.holo_blue_dark));
            upcomingEvents.setPaintFlags(upcomingEvents.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            upcomingEvents.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    final FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container, EventListFragment.getInstance(mPerformer.getSlug(), -1), null).addToBackStack(null).commit();
                }
            });
        } else {
            upcomingEvents.setText(R.string.no_upcoming_events_label);
            upcomingEvents.setTextColor(getActivity().getResources().getColor(android.R.color.holo_red_dark));
            upcomingEvents.setOnClickListener(null);
        }

        return linearLayout;
    }
}
