package com.soulpatch.locl.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.soulpatch.locl.R;
import com.soulpatch.locl.Utils;
import com.soulpatch.locl.datamodel.Event;
import com.soulpatch.locl.network.NetworkResponse;
import com.soulpatch.locl.network.QueryParam;
import com.soulpatch.locl.network.RestCallback;
import com.soulpatch.locl.network.RestCallerTask;

/**
 * Fragment that shows the details of an {@link Event}
 *
 * @author Akshay Viswanathan
 */
public class EventDetailFragment extends Fragment implements OnMapReadyCallback {
    private static final String TAG = EventDetailFragment.class.getName();
    private static final String BUNDLE_ARGS_EVENT = "BUNDLE_ARGS_EVENT";
    private Event mEvent;
    private TextView name;
    private TextView address;
    private TextView type;
    private TextView date;
    private TextView performers;
    private ImageView zoomIn;
    private ImageView zoomOut;
    private ImageView directions;
    private SupportMapFragment mMapFragment;
    private GoogleMap mGoogleMap;
    private LatLng mLocation;
    private float mZoomLevel = 13f;

    public static EventDetailFragment getInstance(final Event event) {
        final Bundle arguments = new Bundle();
        arguments.putSerializable(BUNDLE_ARGS_EVENT, event);
        final EventDetailFragment eventDetailFragment = new EventDetailFragment();
        eventDetailFragment.setArguments(arguments);
        return eventDetailFragment;
    }

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Bundle arguments = getArguments();
        if (arguments == null) {
            throw new IllegalStateException("Event ID needs to be passed to this fragment as a mandatory parameter");
        }

        mEvent = (Event) arguments.getSerializable(BUNDLE_ARGS_EVENT);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        final LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.fragment_event_detail, container, false);
        name = linearLayout.findViewById(R.id.event_name);
        address = linearLayout.findViewById(R.id.event_venue_address);
        type = linearLayout.findViewById(R.id.event_type);
        date = linearLayout.findViewById(R.id.event_date);
        performers = linearLayout.findViewById(R.id.event_performers);
        zoomIn = linearLayout.findViewById(R.id.zoom_in);
        zoomOut = linearLayout.findViewById(R.id.zoom_out);
        directions = linearLayout.findViewById(R.id.directions);
        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.event_map);

        populateEventDetails();

        new RestCallerTask("events/" + mEvent.getId(), new RestCallback() {
            @Override
            public void onResponse(final NetworkResponse response, final Exception exception) {
                Utils.hidProgressDialog();
                if (exception != null || response == null || TextUtils.isEmpty(response.responseData)) {
                    Toast.makeText(getActivity(), "Encountered Error ED1", Toast.LENGTH_LONG).show();
                    if (exception != null) {
                        Log.e(TAG, "ED1:Got an exception " + exception.getMessage());
                        return;
                    }

                    if (response == null) {
                        Log.e(TAG, "Got a null response ");
                    } else if (TextUtils.isEmpty(response.responseData)) {
                        Log.e(TAG, "Got an empty response ");
                    }
                } else {
                    mEvent = new Gson().fromJson(response.responseData, Event.class);
                    populateEventDetails();
                }
            }

            @Override
            public void onPublishProgress(final Integer... values) {
            }

            @Override
            public void onErrorResponse(final NetworkResponse response) {
                Utils.hidProgressDialog();
                Toast.makeText(getActivity(), "Encountered Error ED2", Toast.LENGTH_LONG).show();
                Log.e(TAG, "ED2:onErrorResponse: " + response.responseData);
                Log.e(TAG, "ED2:onErrorResponse: " + response.detail);
            }

            @Override
            public void onPreExecute() {
                Utils.showProgressDialog(getActivity());
            }
        }).execute((QueryParam[]) null);

        return linearLayout;
    }

    private void populateEventDetails() {
        name.setText(mEvent.getTitle());
        address.setText(Utils.getFullAddressString(mEvent.getVenue()));
        final StringBuilder eventType = new StringBuilder();
        final String typeString = mEvent.getType();
        eventType.append(Character.toUpperCase(typeString.charAt(0))).append(typeString.substring(1, typeString.length()));
        type.setText(eventType.toString());
        if (!mEvent.isTimeTbd()) {
            date.setText(Utils.dateFormatter(mEvent.getDateTimeLocal()));
            date.setTextColor(getActivity().getResources().getColor(android.R.color.black));
        } else {
            date.setText(R.string.to_be_determined_label);
            date.setTextColor(getActivity().getResources().getColor(android.R.color.holo_red_dark));
        }
        performers.setText(Utils.getPerformers(mEvent.getPerformers()));
        mMapFragment.onCreate(null);
        mMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mLocation = new LatLng(Double.valueOf(mEvent.getVenue().getLocation().getLat()), Double.valueOf(mEvent.getVenue().getLocation().getLon()));
        // Add a marker for this item and set the camera
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLocation, 13f));
        mGoogleMap.addMarker(new MarkerOptions().position(mLocation));

        // Set the map type back to normal.
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        zoomIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                mZoomLevel += 1f;
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLocation, mZoomLevel));
            }
        });

        zoomOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                mZoomLevel -= 1f;
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLocation, mZoomLevel));
            }
        });

        directions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final String uri = "http://maps.google.com/maps?daddr=" + mLocation.latitude + "," + mLocation.longitude;
                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
        });
    }
}
