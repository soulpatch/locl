package com.soulpatch.locl.ui.components;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * {@link AsyncTask} for loading images from a {@link URL}
 *
 * @author Akshay Viswanathan
 */
public class ImageLoader extends AsyncTask<URL, Integer, Bitmap> {
    private final ImageLoadListener mImageLoadListener;

    public ImageLoader(final ImageLoadListener imageLoadListener) {
        mImageLoadListener = imageLoadListener;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Bitmap doInBackground(final URL... urls) {
        try {
            return BitmapFactory.decodeStream((InputStream) urls[0].getContent());
        } catch (final IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onPostExecute(final Bitmap bitmap) {
        mImageLoadListener.onImageLoaded(bitmap);
    }
}
