package com.soulpatch.locl.ui.components;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.soulpatch.locl.R;
import com.soulpatch.locl.datamodel.Venue;
import com.soulpatch.locl.datamodel.VenueLocation;

import java.util.List;

/**
 * Adapter for {@link RecyclerView} to show a single {@link Venue} detail
 *
 * @author Akshay Viswanathan
 */
public class VenueAdapter extends RecyclerView.Adapter<VenueAdapter.ViewHolder> {
    private final Context mContext;
    private final LoadMoreListener mLoadMoreListener;
    private final List<Venue> mValues;

    public VenueAdapter(final Context context, final List<Venue> events, final LoadMoreListener loadMoreListener) {
        mContext = context;
        mValues = events;
        mLoadMoreListener = loadMoreListener;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final CardView cardView = (CardView) LayoutInflater.from(mContext).inflate(R.layout.cardview_venue, parent, false);
        return new ViewHolder(cardView);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Venue venue = mValues.get(position);
        final VenueLocation location = venue.getLocation();
        holder.initializeMapView();
        holder.name.setText(venue.getName());
        holder.address.setText(mContext.getString(R.string.address, venue.getName(), venue.getAddress(), venue.getCity()));
        if (!venue.hasUpcomingEvents()) {
            holder.hasUpcomingEvents.setText(R.string.no_upcoming_events);
            holder.hasUpcomingEvents.setTypeface(null, Typeface.ITALIC);
        }

        holder.mapView.setTag(location);

        if (holder.map != null) {
            // The map is ready to be used
            setMapLocation(holder.map, location);
        }

        if (position == getItemCount() - 1) {
            mLoadMoreListener.onLoadMore();
        }

        holder.mRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final FragmentTransaction fragmentTransaction = ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container, VenueDetailFragment.getInstance(venue), null).addToBackStack(null).commit();
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {
        private final CardView mRootView;
        private final TextView name;
        private final TextView address;
        private final TextView hasUpcomingEvents;
        private final MapView mapView;
        GoogleMap map;

        ViewHolder(final View rootView) {
            super(rootView);
            mRootView = (CardView) rootView;
            name = rootView.findViewById(R.id.venue_name);
            address = rootView.findViewById(R.id.venue_address);
            hasUpcomingEvents = rootView.findViewById(R.id.venue_has_upcoming_events);
            mapView = rootView.findViewById(R.id.map_view);
        }

        @Override
        public void onMapReady(final GoogleMap googleMap) {
            MapsInitializer.initialize(mContext);
            map = googleMap;
            final VenueLocation data = (VenueLocation) mapView.getTag();
            if (data != null) {
                setMapLocation(map, data);
            }
        }

        /**
         * Initialises the MapView by calling its lifecycle methods.
         */
        void initializeMapView() {
            if (mapView != null) {
                // Initialise the MapView
                mapView.onCreate(null);
                // Set the map ready callback to receive the GoogleMap object
                mapView.getMapAsync(this);
            }
        }
    }

    /**
     * Displays a {@link VenueLocation} on a
     * {@link GoogleMap}.
     * Adds a marker and centers the camera on the NamedLocation with the normal map type.
     */
    private static void setMapLocation(final GoogleMap map, final VenueLocation data) {
        //TODO change the VenueLocation to automatically convert the location data coming in JSON to Doubles at the timer of parsing
        final LatLng location = new LatLng(Double.valueOf(data.getLat()), Double.valueOf(data.getLon()));
        // Add a marker for this item and set the camera
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 13f));
        map.addMarker(new MarkerOptions().position(location));

        // Set the map type back to normal.
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }
}