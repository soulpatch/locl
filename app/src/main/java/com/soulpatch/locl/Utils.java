package com.soulpatch.locl;

import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;

import com.soulpatch.locl.datamodel.Performer;
import com.soulpatch.locl.datamodel.Venue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * @author Akshay Viswanathan
 */

public class Utils {
    private static ProgressDialog smProgressDialog;

    public static void showProgressDialog(final Context context) {
        if (smProgressDialog != null) {
            smProgressDialog.cancel();
            smProgressDialog.dismiss();
            smProgressDialog = null;
        }

        smProgressDialog = new ProgressDialog(context);
        smProgressDialog.setMessage("Loading...");
        smProgressDialog.show();
    }

    public static void hidProgressDialog() {
        if (smProgressDialog == null) {
            return;
        }
        smProgressDialog.cancel();
        smProgressDialog.dismiss();
    }

    public static String getFullAddressString(final Venue venue) {
        final StringBuilder stringBuilder = new StringBuilder();
        if (venue == null) {
            return stringBuilder.toString();
        }

        if (!TextUtils.isEmpty(venue.getAddress())) {
            stringBuilder.append(venue.getAddress());
        }

        if (!TextUtils.isEmpty(venue.getExtendedAddress())) {
            stringBuilder.append(",").append(venue.getExtendedAddress());
        }

        if (!TextUtils.isEmpty(venue.getCity())) {
            stringBuilder.append(", ").append(venue.getCity());
        }

        if (!TextUtils.isEmpty(venue.getState())) {
            stringBuilder.append(" ").append(venue.getState());
        }

        if (!TextUtils.isEmpty(venue.getPostalCode())) {
            stringBuilder.append(" ").append(venue.getPostalCode());
        }

        return stringBuilder.toString();
    }

    public static String getPerformers(final List<Performer> performers) {
        final StringBuilder stringBuilder = new StringBuilder();
        if (performers == null || performers.isEmpty()) {
            return stringBuilder.toString();
        }

        for (final Performer performer : performers) {
            stringBuilder.append(performer.getName()).append(",");
        }

        return stringBuilder.toString().substring(0, stringBuilder.length() - 1);
    }

    public static String dateFormatter(final String date) {
        if (TextUtils.isEmpty(date)) {
            return "Date and Time to be determined";
        }

        final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            final Date formattedDate = format.parse(date);
            format.applyPattern("dd MMM yyyy HH:mm");
            return format.format(formattedDate) + " Hrs";
        } catch (final ParseException pe) {
            pe.printStackTrace();
        }
        return null;
    }
}
